package network;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.ObjectInput;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.DatagramPacket;
import java.net.InetAddress;
import java.net.Socket;

import controller.ServidorController;
import model.Paciente;
import util.Acao;

public class Gerenciador implements Runnable{

    private DatagramPacket pacote;
    private InetAddress ip;
    private int porta;
    private Object mensagem;
    private Socket socketServidor;

    /**
     * Construtor para requisições via UDP
     * @param pacote - o pacote recebido
     */
    public Gerenciador(DatagramPacket pacote){
        this.pacote = pacote;
        this.ip = pacote.getAddress();
        this.porta = pacote.getPort();
        mensagem = desserializarMensagem(pacote.getData());

    }

    /**
     * Construtor para requisições via TCP
     * @param socket - O socket da conexão
     */
    public Gerenciador(Socket socket){
        this.socketServidor = socket;
        try {
            InputStream tcpInput = new ObjectInputStream(socketServidor.getInputStream());

            ByteArrayOutputStream bytes = new ByteArrayOutputStream();
            ObjectOutputStream obj = new ObjectOutputStream(bytes);
            
            try {
                obj.writeObject(((ObjectInputStream) tcpInput).readObject());
                bytes.toByteArray();
                mensagem = desserializarMensagem(bytes.toByteArray());
            } catch (ClassNotFoundException e) {

            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
    
    /**
     * Transforma um array de bytes em um Object.
     * 
     * @param data - O array de bytes recebido.
     * @return Object - O objeto desserializado.
     */
    private Object desserializarMensagem(byte[] data) {
        ByteArrayInputStream mensagem = new ByteArrayInputStream(data);

        try {
            ObjectInput leitor = new ObjectInputStream(mensagem);
            return (Object)leitor.readObject();
        } catch (IOException | ClassNotFoundException e) {
            e.printStackTrace();
        }

        return null;
    }

    @Override
    public void run() {
        
        /*Define as decisões a serem tomadas a partir das requisições recebidas*/
        
        if(mensagem instanceof Acao){
            Acao acaoRequisitada = (Acao) mensagem; // A ação recebida

            if(acaoRequisitada.getAcao().equals("--connect")){
                ServidorController.getInstance().responderSensorTCP(socketServidor,"accepted");
            }

            else if(acaoRequisitada.getAcao().equals("--ping")){
                String idCliente = (String) acaoRequisitada.getObjeto();
                ServidorController.getInstance().responderPingTCP(socketServidor,"ok",idCliente);
            }

            else if(acaoRequisitada.getAcao().equals("--save")){
                Paciente paciente = (Paciente) acaoRequisitada.getObjeto();
                ServidorController.getInstance().salvarPaciente(paciente, ip, porta);
            }

            else if(acaoRequisitada.getAcao().equals("--login")){
                String[] credenciais = ((String) acaoRequisitada.getObjeto()).split(":");
                ServidorController.getInstance().salvarMedico(credenciais[2], socketServidor.getInetAddress(), socketServidor.getPort());
                ServidorController.getInstance().logar(credenciais[0],credenciais[1],socketServidor);
            }

            else if(acaoRequisitada.getAcao().equals("--list")){

                if(acaoRequisitada.getObjeto() instanceof String){

                    String filtro = (String)acaoRequisitada.getObjeto();

                    if(filtro.equals("-all")){
                        ServidorController.getInstance().listarTodos(this.ip,this.porta);
                    }else if(filtro.startsWith("-selected")){
                        String id = filtro.substring(filtro.indexOf(":")+1);
                        ServidorController.getInstance().listarSelecionados(id, ip, porta);
                    }else if(filtro.equals("-danger")){
                        ServidorController.getInstance().listarPropensos(ip, porta);
                    }
                }
            }

            else if(acaoRequisitada.getAcao().equals("--select")){

                if(acaoRequisitada.getObjeto() instanceof String){
                    String[] credenciais = ((String) acaoRequisitada.getObjeto()).split(":");					
                    ServidorController.getInstance().selecionarPaciente(credenciais[0],credenciais[1],ip,porta);
                }
            }

            else if(acaoRequisitada.getAcao().equals("--watch")){

                if(acaoRequisitada.getObjeto() instanceof String){
                    String[] credenciais = ((String) acaoRequisitada.getObjeto()).split(":");					
                    ServidorController.getInstance().atualizarPacienteEspecifico(credenciais[1],ip,porta); //(idMedico,idPaciente)
                }
            }

            else if(acaoRequisitada.getAcao().equals("--logout")){
                String[] credenciais = ((String)acaoRequisitada.getObjeto()).split(":");
                ServidorController.getInstance().logout(credenciais[0],credenciais[1],socketServidor); //[0] = tipoCliente [1] = idCliente 
            }

            else if(acaoRequisitada.getAcao().equals("--history")){
                ServidorController.getInstance().recuperarHistorico((String)acaoRequisitada.getObjeto(),socketServidor);
            }
        }

    }
	

}
