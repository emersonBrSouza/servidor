package controller;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.io.ObjectOutput;
import java.io.ObjectOutputStream;
import java.io.PrintStream;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetAddress;
import java.net.Socket;
import java.net.SocketException;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardOpenOption;
import java.nio.file.attribute.FileTime;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Scanner;
import java.util.concurrent.ConcurrentHashMap;

import model.Conexao;
import model.Paciente;
import model.Sensor;
import util.MensagemUDP;

public class ServidorController {

    

    //Declaração das TAD's
    private Map<String,Conexao> conexoes = new ConcurrentHashMap<String,Conexao>(); // Armazena as conexões com os clientes <idCliente,Conexao>
    private Map<String,Paciente> pacientesPropensos = new ConcurrentHashMap<String,Paciente>(); // Armazena os pacientes propensos à sofrer um ataque cardíaco <idPaciente,Paciente>
    private Map<String,ArrayList<Paciente>> pacientesSelecionados = new ConcurrentHashMap<String,ArrayList<Paciente>>(); //Armazena os pacientes selecionados por cada médico

    //Singleton
    private static ServidorController controller;
    private ServidorController(){}
    public static ServidorController getInstance(){
            if(controller == null){
                    controller = new ServidorController();
            }
            return controller;
    }
	
	
    //Métodos de Envio

    /**
     * Envia uma mensagem usando UDP.
     * @param mensagem MensagemUDP - A mensagem a ser enviada.
     */
    public synchronized void enviarUDP(MensagemUDP mensagem){
            try {
                    DatagramSocket socket = new DatagramSocket();
                    byte[] msg = serializarMensagens(mensagem); // Transforma a mensagem em um array de bytes

                    DatagramPacket dados = new DatagramPacket(msg,msg.length,mensagem.getDestinoIP(),mensagem.getDestinoPorta()); //Prepara a conexão
                    socket.send(dados);// Envia os dados
            } catch (SocketException e) {
                    e.printStackTrace();
            } catch (IOException e) {
                    e.printStackTrace();
            }

    }

    /**
     * Envia uma mensagem para o sensor usando TCP.
     * 
     * @param socket - O Socket da conexão
     * @param mensagem - A mensagem a ser enviada
     */
    public synchronized void responderSensorTCP(Socket socket,String mensagem) {
            try {
                    ObjectOutputStream saida = new ObjectOutputStream(socket.getOutputStream());
                    saida.writeObject(mensagem);
                    saida.flush();
            } catch (IOException e) {
                    e.printStackTrace();
            }

    }

    /**
     * Envia a resposta para o cliente que enviou "Ping".
     *
     * @param socket - O socket da conexão
     * @param mensagem - A mensagem a ser enviada.
     * @param idCliente - O id do cliente destinatário.
     */
    public synchronized void responderPingTCP(Socket socket,String mensagem,String idCliente) {

        Conexao c = conexoes.get(idCliente); // Obtém o cliente desejado

        if(c != null){ // Verifica se a conexão não é nula
                conexoes.get(idCliente).setUltimaAtualizacao(System.currentTimeMillis()); //Registra o momento da solicitação recebida pelo servidor.
        }

        try {
                ObjectOutputStream saida = new ObjectOutputStream(socket.getOutputStream());
                saida.writeObject(mensagem);
                saida.flush();
        } catch (IOException e) {
                e.printStackTrace();
        }

    }
	
    //Métodos de Histórico

    /**
     * Salva o histórico dos momentos em que o paciente esteve propenso.
     *
     * @param Paciente - O paciente propenso.
     */
    private synchronized void salvarHistorico(Paciente paciente){
        Path pasta = Paths.get("history").toAbsolutePath(); //Obtém o caminho da pasta
        try {
            if(Files.notExists(pasta)){ // Verifica se a pasta não existe.
                Files.createDirectory(pasta); //Caso não exista, a pasta é criada
            }
        } catch (IOException e1) {
            e1.printStackTrace();
        }

        Path pastaRaiz = Paths.get("history",paciente.getSensor().getId()+".txt").toAbsolutePath(); //Obtém o arquivo do histórico

        SimpleDateFormat dtf = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");// Cria uma máscara para a data e o horário
        Date date = new Date();

        //Guarda as informações do paciente
        List<String> dados = Arrays.asList("horario["+dtf.format(date)+"]",
                                            "batimentos["+paciente.getSensor().getBatimentos()+"]",
                                            "pressaoSistolica["+paciente.getSensor().getPressaoSistolica()+"]",
                                            "pressaoDiastolica["+paciente.getSensor().getPressaoDiastolica()+"]",
                                            "emRepouso["+paciente.getSensor().estaEmRepouso()+"]",
                                            "}}");


        try {
            if(Files.notExists(pastaRaiz)){ //Cria o arquivo se ele não existir
                Files.createFile(pastaRaiz);
            }
            FileTime ultimaModificacao = Files.getLastModifiedTime(pastaRaiz); //Obtém o tempo da última modificação do arquivo

            if(System.currentTimeMillis() - ultimaModificacao.toMillis() > 20000){ //O arquivo é atualizado a cada vinte segundos.
                Files.write(pastaRaiz, dados, Charset.forName("UTF-8"),StandardOpenOption.APPEND); // Escreve no fim do arquivo
            }
        } catch (IOException e) {
                e.printStackTrace();
        }

    }

    /**
     * Recupera o histórico de um paciente.
     * 
     * 
     * @param idPaciente - O ID do paciente a ser recuperado.
     * @param socket - O socket da conexão.
     */
    public synchronized void recuperarHistorico(String idPaciente, Socket socket) {
        Path pastaRaiz = Paths.get("history",idPaciente+".txt").toAbsolutePath(); //Obtém o caminho do arquivo

        if(Files.exists(pastaRaiz)){// Verifica se o arquivo existe
            File file = new File(pastaRaiz.normalize().toString()); //Obtém o arquivo

            try {
                ArrayList<String[]> historico = new ArrayList<String[]>(); // Lista do para armazenar as informações do histórico
                ObjectOutputStream saida = new ObjectOutputStream(socket.getOutputStream());
                Scanner leitor = new Scanner(file);
                leitor.useDelimiter("}}"); // Separador de dados


                while(leitor.hasNext()){ // Percorre o arquivo e guarda os dados na lista do histórico
                        String[] dados = new String[5];
                        String s = leitor.next();
                        String[] cred = s.split("\\[");

                        for(int i=1;i<cred.length;i++){
                                dados[i-1] = cred[i].substring(0, cred[i].indexOf("]"));
                                /*
                                 * [0] - horario
                                 * [1] - batimentos
                                 * [2] - pressão sistólica
                                 * [3] - pressão diastólica
                                 * [4] - em repouso*/
                        }
                        historico.add(dados);
                }
                
                //Envia o arquivo
                saida.writeObject(historico);
                saida.flush();
            } catch (IOException e) {
                    e.printStackTrace();
            }

        }
    }

    //Rotinas de Monitoramento

    /**
     * Monitora frequentemente a lista de usu�rios para verificar o estado de cada um.<br>
     * Faixa de valores para verifica��es.<br>
     * Batimentos (em movimento)- 
     * <b>Alto:</b> valor > 140
     * <b>Normal:</b> valor = 100
     * <b>Baixo:</b> valor < 80 <br>
     * 
     * Press�o Sist�lica (em cmHg) / Press�o Diast�lica (em cmHg) - 
     * <b>Alta:</b> valor >= 14 / 10
     * <b>Normal:</b> valor = 12/8 - 13/9
     * <b>Baixa:</b> valor <= 9/6 <br>
     * */
    public void monitorarPacientes(){
        new Thread(){
            public void run(){
                while(true){
                    pacientesPropensos = new ConcurrentHashMap<String,Paciente>();
                    Iterator<String> iterador = conexoes.keySet().iterator();

                    while(iterador.hasNext()){ // Itera a lista de conexões e verifica as condições de saúde do paciente
                        Conexao c = conexoes.get(iterador.next());
                        if(c != null){
                            if(c.getPaciente() != null){
                                Sensor s = c.getSensor();
                                boolean pressaoAlta = s.getPressaoSistolica() >= 14 && s.getPressaoDiastolica() >= 9;
                                boolean pressaoBaixa = s.getPressaoSistolica() <=9 && s.getPressaoDiastolica() <= 6;

                                boolean bpmAltoMovimento = s.getBatimentos() > 140 &&  !s.estaEmRepouso();
                                boolean bpmBaixoMovimento = s.getBatimentos() < 80 && !s.estaEmRepouso();

                                /*No caso de alguma das condições abaixo ser satisfeita, o paciente é considerado propenso e 
                                  a informação é salva no histórico*/
                                if( s.estaEmRepouso() && s.getBatimentos() < 40){
                                        pacientesPropensos.put(c.getId(), c.getPaciente());
                                        salvarHistorico(c.getPaciente());
                                } 

                                else if(pressaoAlta){
                                        pacientesPropensos.put(c.getId(), c.getPaciente());
                                        salvarHistorico(c.getPaciente());
                                } 

                                else if(pressaoBaixa){
                                        pacientesPropensos.put(c.getId(), c.getPaciente());
                                        salvarHistorico(c.getPaciente());
                                } 

                                else if(s.estaEmRepouso() && s.getBatimentos() >= 100){
                                        pacientesPropensos.put(c.getId(), c.getPaciente());
                                        salvarHistorico(c.getPaciente());
                                } 

                                else if(bpmAltoMovimento){
                                        pacientesPropensos.put(c.getId(), c.getPaciente());
                                        salvarHistorico(c.getPaciente());
                                } 

                                else if(bpmBaixoMovimento){
                                        pacientesPropensos.put(c.getId(), c.getPaciente());
                                        salvarHistorico(c.getPaciente());
                                } 
                            }
                        }

                    }
                }
            }
        }.start();
    }

    /**
     * Verifica se o cliente realizou alguma requisição no último minuto, caso isso não tenha ocorrido, o mesmo é retirado da lista de conexões
     */
    public void atualizarConectados(){
        new Thread(){
            @Override
            public void run(){
                while(true){
                    Iterator<String> i = conexoes.keySet().iterator();

                    while(i.hasNext()){ //Percorre a lista de clientes e verifica se os mesmos fizeram alguma requisição
                        String chaveAtual = i.next();
                        long tempoAtual = System.currentTimeMillis(); //Obtém o tempo atual
                        Conexao c = conexoes.get(chaveAtual);
                        long tempoUltima = 0;
                        if(c != null){
                            tempoUltima = c.getUltimaAtualizacao(); //Obtém a última atualização do cliente
                        }

                        if(tempoAtual - tempoUltima > 60000){ //Caso a comparação seja verdadeira, o cliente é retirado da lista de conexões.
                                logoutForcado(conexoes.get(chaveAtual).getTipoCliente(), chaveAtual);
                                System.out.println("Desconectando: "+chaveAtual);
                        }
                    }
                }
            }
        }.start();
    }
	
	
    //Métodos de Listagem

    /**
     * Adiciona um pacientes à uma lista de pacientes selecionados por um médico.
     * 
     * @param idMedico - O ID do médico que selecionou o paciente.
     * @param idPaciente - O ID do paciente selecionado.
     * @param ip - O endereço IP que receberá a resposta do servidor
     * @param porta - A porta do cliente que receberá a resposta do servidor
     * */
    public synchronized void selecionarPaciente(String idMedico, String idPaciente, InetAddress ip, int porta) {

        Paciente p = conexoes.get(idPaciente).getPaciente();

        ArrayList<Paciente> pacientes = pacientesSelecionados.get(idMedico); // Obtém a lista de pacientes selecionados por um médico.

        if(pacientes == null || pacientes.size() == 0 ){ // Se a lista não existir, uma nova é criada
                ArrayList<Paciente> selecao = new ArrayList<Paciente>();
                selecao.add(p); // Adiciona o paciente à lista.
                pacientesSelecionados.put(idMedico,selecao); // Associa a lista a um médico
        }else{
                pacientesSelecionados.get(idMedico).add(p); // Associa a lista a um médico
        } 

        listarSelecionados(idMedico, ip, porta); //Envia a lista de pacientes selecionados para o médico.
    }

    /**
     * Envia a lista de todos os pacientes conectados para o médico.
     * 
     * @param ip - O IP do médico que receberá a lista.
     * @param porta - A porta do médico que receberá a lista.
     */
    public synchronized void listarTodos(InetAddress ip, int porta){
        ArrayList<Paciente> pacientes = new ArrayList<Paciente>();
        Iterator<String> iterador= conexoes.keySet().iterator();

        while(iterador.hasNext()){ // Percorre a lista de conexões para selecionar os pacientes conectados.
            Conexao c = conexoes.get(iterador.next()); // Obtém uma conexão
            if(c != null){
                    if(c.getPaciente() != null){ // Se for um paciente , adiciona-o à lista.
                            pacientes.add(c.getPaciente());
                    }
            }
            
        }
        
        enviarUDP(new MensagemUDP(pacientes,ip,porta,"--all"));//Envia a resposta para o cliente.
    }

    /**
     * Envia a lista de todos os pacientes selecionados por um médico.
     * 
     * @param idMedico - O ID do médico que receberá a lista.
     * @param ip - O IP do médico que receberá a lista.
     * @param porta - A porta do médico que receberá a lista.
     */
    public synchronized void listarSelecionados(String idMedico, InetAddress ip, int porta){

        ArrayList<Paciente> lista = pacientesSelecionados.get(idMedico);
        if(lista == null){ // Verifica se a lista existe.
                return;
        }
        
        //Iterator<Paciente> iterador = lista.iterator();

        /*while(iterador.hasNext()){ //Percorre a lista 
            Paciente atual = iterador.next();
            Conexao conexao = conexoes.get(atual.getSensor().getId());
            if(conexao != null && conexao.getTipoCliente().equals("Paciente")){
                Sensor conectado = conexao.getSensor();
                atual.setSensor(conectado);
            }
        }*/

        enviarUDP(new MensagemUDP(lista,ip,porta,"--selected")); //Envia a resposta para o médico
    }

    /**
     * Envia a lista de todos os pacientes propensos à um ataque cardíaco para um médico.
     *
     * @param ip - O IP do médico que receberá a lista.
     * @param porta - A porta do médico que receberá a lista.
     */
    public synchronized void listarPropensos(InetAddress ip,int porta){
        ArrayList<Paciente> pacientes = new ArrayList<Paciente>();
        Iterator<String> iterador= pacientesPropensos.keySet().iterator();

        while(iterador.hasNext()){ //Itera o Map de pacientes propensos, e adiciona-os à um ArrayList para serem enviados.
            Paciente p = pacientesPropensos.get(iterador.next());
            pacientes.add(p);
        }

        enviarUDP(new MensagemUDP(pacientes,ip,porta,"--danger")); //Envia a resposta para o médico
    }

    /**
     * Atualiza os dados de um paciente específico que está sendo monitorado.
     * 
     * @param idPaciente - O ID do paciente monitorado.
     * @param ip - O IP do médico que receberá a resposta.
     * @param porta - A porta do médico que receberá a resposta.
     */
    public synchronized void atualizarPacienteEspecifico(String idPaciente, InetAddress ip, int porta) {

        Conexao c = conexoes.get(idPaciente);

        if(c != null){
            Paciente paciente = conexoes.get(idPaciente).getPaciente(); // Obtém o paciente
            enviarUDP(new MensagemUDP(paciente,ip,porta,"--single")); // Envia a resposta para o médico.
        }
    }
	
    //Métodos Auxiliares

    /**
     * Transforma uma mensagem em um array de bytes.
     * 
     * @param mensagem - A mensagem a ser transformada.
     * @return byte[] - Um array de bytes convertidos
     */
    public byte[] serializarMensagens(Object mensagem){
        ByteArrayOutputStream b = new ByteArrayOutputStream();
        try {
            //Transforma a mensagem em um byte array
            ObjectOutput out = new ObjectOutputStream(b); 
            out.writeObject(mensagem);
            out.flush();
            return b.toByteArray();
        } catch (IOException e) {
            System.err.println("Erro no envio/recebimento dos dados");
        }
        return null;

    }
	
    //Métodos de Login - Logout

    /**
     * Cria/Atualiza uma conexão com o paciente.
     * 
     * @param paciente - O paciente a ser conectado.
     * @param ip - O IP do paciente.
     * @param porta - A porta do paciente.
     */
    public synchronized void salvarPaciente(Paciente paciente,InetAddress ip, int porta){
        Conexao conexao = new Conexao(paciente.getSensor().getId(),paciente,ip,porta); // Cria uma nova conexão
        conexao.setUltimaAtualizacao(System.currentTimeMillis()); // Define o horário da última atualização
        conexoes.put(paciente.getSensor().getId(), conexao); //Adiciona a conexão ao Map
    }

     /**
     * Cria/Atualiza uma conexão com o médico.
     * 
     * @param id - O ID do médico a ser conectado.
     * @param ip - O IP do paciente.
     * @param porta - A porta do paciente.
     */
    public synchronized void salvarMedico(String id,InetAddress ip, int porta){
        Conexao conexao = new Conexao(id,ip,porta); // Cria uma nova conexão
        conexao.setUltimaAtualizacao(System.currentTimeMillis()); // Define o horário da última atualização
        conexoes.put(id, conexao); //Adiciona a conexão ao Map
    }

    /**
     * Realiza o login de um médico.
     * 
     * @param username - O nome de usuário do médico
     * @param password - A senha do médico
     * @param socket - O socket da conexão
     */
    public synchronized void logar(String username, String password,Socket socket){
        try {
            if(acessarConta(username,password)){ //Verifica se os dados estão corretos
                //Enviam uma resposta permitindo o acesso.
                PrintStream saida = new PrintStream(socket.getOutputStream(),true);
                saida.flush();
                saida.println("--login-accepted");

            }else{
                 //Enviam uma resposta negando o acesso.
                PrintStream saida = new PrintStream(socket.getOutputStream(),true);
                saida.flush();
                saida.println("--login-refused");
            }
            socket.close();//Fecha o socket.
        } catch (IOException e) {
                e.printStackTrace();
        }
    }

    /**
     * Busca as credenciais no arquivo de cadastros.
     * 
     * @param username - O nome de usuário do médico
     * @param password - A senha do médico
     * @return true - Se os dados corresponderem
     * @return false - Se os dados não corresponderem
     */
    private synchronized boolean acessarConta(String username, String password){	
        String fs = System.getProperty("file.separator");
        Scanner bd = new Scanner(getClass().getResourceAsStream("/bd.txt"));

        while(bd.hasNext()){ //Procura no arquivo usuário e senha correspondentes aos informados
            String texto = bd.nextLine();
            String[] partes = {texto.substring(texto.indexOf("#")+1,texto.indexOf(":")),texto.substring(texto.indexOf(":")+1, texto.indexOf(";"))};
            if(username.equals(partes[0]) && password.equals(partes[1])){
                bd.close();
                return true;
            }
        }
        bd.close();
        return false;
    }

    /**
     * Forçar a desconexão de um cliente. Não envia resposta ao cliente.
     * 
     * @param tipo - O tipo do cliente.
     * @param idProcurado - O ID do cliente a ser removido.
     *      */
    private synchronized void logoutForcado(String tipo, String idProcurado){
        if(tipo.equals("Médico")){ 
            pacientesSelecionados.remove(idProcurado); // Remove a lista de pacientes selecionados por um médico
            removerConexao(tipo,idProcurado); //Remove a conexão

        }else if(tipo.equals("Paciente")){
            pacientesPropensos.remove(idProcurado); //Remove o paciente da lista de propensos.

            Iterator<String> iterador = pacientesSelecionados.keySet().iterator();

            Paciente temp = null;

            while(iterador.hasNext()){ //Percorre o Map de médicos
                String idExistente = iterador.next();
                ArrayList<Paciente> pacientes = pacientesSelecionados.get(idExistente); //Recupera a lista de pacientes de um m�dico
                Iterator<Paciente> iteradorLista = pacientes.iterator();

                while(iteradorLista.hasNext()){ //Percorre a lista de pacientes de um m�dico
                        Paciente atual = iteradorLista.next();
                        if(atual.getSensor().getId().equals(idProcurado)){
                                temp = atual; //Guarda a refer�ncia do paciente desejado
                        }
                }

                if(temp !=null){
                        pacientes.remove(temp);
                }

            }

            removerConexao(tipo,idProcurado);			
        }
    }

    /**
     * Realiza o logout de um cliente. Envia resposta ao cliente.
     * 
     * @param tipo - O tipo do cliente.
     * @param idProcurado - O ID do cliente a ser removido.
     * @param socket - O socket a ser removido.
     */
    public synchronized void logout(String tipo,String idProcurado,Socket socket) {
        
        if(tipo.equals("Médico")){ // Verifica se o cliente é um médico
            pacientesSelecionados.remove(idProcurado); // Remove a lista de pacientes selecionados por um médico
            removerConexao(tipo,idProcurado); //Remove a conexão

            // Envia a resposta para o médico
            try {
                PrintStream saida = new PrintStream(socket.getOutputStream(),true);
                saida.flush();
                saida.println("--logout-accepted");
            } catch (IOException e) {
                System.err.println("Erro de I/O");
            }
            
        }else if(tipo.equals("Paciente")){ // Verifica se o cliente é um paciente
            pacientesPropensos.remove(idProcurado); //Remove o paciente da lista de propensos.

            Iterator<String> iterador = pacientesSelecionados.keySet().iterator();

            Paciente temp = null;

            while(iterador.hasNext()){ //Percorre o Map de médicos
                String idExistente = iterador.next();
                ArrayList<Paciente> pacientes = pacientesSelecionados.get(idExistente); //Recupera a lista de pacientes de um médico
                Iterator<Paciente> iteradorLista = pacientes.iterator();

                while(iteradorLista.hasNext()){ //Percorre a lista de pacientes de um médico
                    Paciente atual = iteradorLista.next();
                    if(atual.getSensor().getId().equals(idProcurado)){
                        temp = atual; //Guarda a referência do paciente desejado
                    }
                }

                if(temp !=null){
                    pacientes.remove(temp); //Remove o paciente, caso ele exista na lista.
                }

            }

            // Remove a conexão
            removerConexao(tipo,idProcurado);

            // Envia a resposta para o cliente.
            try {
                    PrintStream saida = new PrintStream(socket.getOutputStream(),true);
                    saida.flush();
                    saida.println("--logout-accepted");
            } catch (IOException e) {
                    System.err.println("Erro de I/O");
            }

        }

    }

    /**
     * Remove uma conexão existente.
     * 
     * @param tipo - O tipo do cliente.
     * @param idProcurado - O ID do cliente a ser removido.
     */
    private synchronized void removerConexao(String tipo,String idProcurado){
        Iterator<String> iterador = conexoes.keySet().iterator();
        while(iterador.hasNext()){ // Percorre a lista de conexões e remove a mesma se a condição a seguir for satisfeita
            String idExistente = iterador.next();
            Conexao conexao = conexoes.get(idExistente);

            if(conexao.getTipoCliente().equals(tipo) && conexao.getId().equals(idProcurado)){ //Se a condição for satisfeita, a conexão é removida
                    conexoes.remove(idExistente);
            }
        }
    }


}
